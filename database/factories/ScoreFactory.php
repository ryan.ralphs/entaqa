<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Score::class, function (Faker $faker) {
    return [
       'repository_id' => factory(App\Models\CodeRepository::class)->create()->id,
        'code' => $faker->randomFloat(1, 0, 3),
        'complexity' => $faker->randomFloat(1, 0, 3),
        'architecture' => $faker->randomFloat(1, 0, 3),
        'style' => $faker->randomFloat(1, 0, 3),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),

    ];
});
