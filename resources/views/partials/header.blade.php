<header class="header">
    <div class="navigation-trigger hidden-xl-up" data-ma-action="aside-open" data-ma-target=".sidebar">
        <div class="navigation-trigger__inner">
            <i class="navigation-trigger__line"></i>
            <i class="navigation-trigger__line"></i>
            <i class="navigation-trigger__line"></i>
        </div>
    </div>

    <div class="header__logo hidden-sm-down">
        <h1 style="text-align: center;"><a href="{{ env('APP_URL') }}">
                {{ config('app.name') }}</a></h1>
    </div>

    <form class="search">
        <div class="search__inner">
            <input type="text" class="search__text" placeholder="Search for people, files, documents...">
            <i class="zmdi zmdi-search search__helper" data-ma-action="search-close"></i>
        </div>
    </form>

    <ul class="top-nav">
        <li class="hidden-xl-up"><a href="" data-ma-action="search-open"><i class="zmdi zmdi-search"></i></a></li>

        <li class="dropdown top-nav__notifications">
            <a href="" data-toggle="dropdown" class="top-nav__notify">
                <i class="zmdi zmdi-notifications"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu--block">
                <div class="listview listview--hover">
                    <div class="listview__header">
                        Notifications

                        <div class="actions">
                            <a href="" class="actions__item zmdi zmdi-check-all" data-ma-action="notifications-clear"></a>
                        </div>
                    </div>

                    <div class="p-1"></div>
                </div>
            </div>
        </li>

        <li class="dropdown hidden-xs-down">
            <a href="" data-toggle="dropdown"><i class="zmdi zmdi-apps"></i></a>

            <div class="dropdown-menu dropdown-menu-right dropdown-menu--block" role="menu">
                <div class="row app-shortcuts">
                    <a class="col-4 app-shortcuts__item" href="{{ env('UNIFY_AUTH_URL') }}">
                        <i class="zmdi zmdi-wrench"></i>
                        <small class="">Settings</small>
                        <span class="app-shortcuts__helper bg-red"></span>
                    </a>
                    <a class="col-4 app-shortcuts__item" href="{{ env('UNIFY_SYNERGI_URL') }}">
                        <i class="zmdi zmdi-shopping-basket"></i>
                        <small class="">Synergi</small>
                        <span class="app-shortcuts__helper bg-blue"></span>
                    </a>
                    <a class="col-4 app-shortcuts__item" href="{{ env('UNIFY_VOIP_URL') }}">
                        <i class="zmdi zmdi-phone"></i>
                        <small class="">VoIP</small>
                        <span class="app-shortcuts__helper bg-light-green"></span>
                    </a>
                </div>
            </div>
        </li>
    </ul>
</header>