@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ $repo->name }}</div>

                <div class="card-body">
                   {{ $scores->code }}
                    {{ $scores->complexity }}
                    {{ $scores->architecture }}
                    {{ $scores->style }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
