<?php

namespace Tests\Unit;

use App\Models\CodeRepository;
use App\Models\Score;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ScoreTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_scores_belong_to_a_repository()
    {
       $score =  factory(Score::class)->create();

       $this->assertInstanceOf(CodeRepository::class, $score->codeRepository);

    }
}
