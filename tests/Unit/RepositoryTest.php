<?php

namespace Tests\Unit;

use App\Models\CodeRepository;
use App\Models\Score;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RepositoryTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_repositories_have_scores()
    {
       $repository = factory(CodeRepository::class)->create();
       $repository->wasRecentlyCreated = false;

       $score = factory(Score::class)->create(['repository_id' => $repository]);

       $this->assertEquals($repository, $score->codeRepository);
    }
}
