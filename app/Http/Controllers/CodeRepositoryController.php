<?php

namespace App\Http\Controllers;

use App\Models\CodeRepository;
use App\Models\Score;
use Illuminate\Http\Request;

class CodeRepositoryController extends Controller
{
    public function show($repository)
    {
        $repo = CodeRepository::where('name', $repository)->first();

        $scores = Score::where('repository_id', $repo->id)->first();


        return view('partials.codebase', ['repo' => $repo, 'scores' => $scores]);
    }
}
