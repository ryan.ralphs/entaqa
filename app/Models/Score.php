<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    protected $guarded = [];
    protected $table = 'scores';


    public function codeRepository()
    {
        return $this->belongsTo(CodeRepository::class, 'repository_id');
    }
}
