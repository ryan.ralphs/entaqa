<?php

namespace App\Models;

use App\UserSetting;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     * Returns the social accounts associated with the user
     */
    public function accounts()
    {
        return $this->hasMany(LinkedSocialAccount::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     * Returns the settings associated with the user
     */
    public function settings()
    {
        return $this->hasOne(UserSetting::class);
    }
}
