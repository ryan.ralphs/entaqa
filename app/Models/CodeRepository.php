<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CodeRepository extends Model
{
    protected $guarded = [];
    protected $table = 'code_repositories';

    public function scores()
    {
        return $this->hasMany(Score::class);
    }
}
