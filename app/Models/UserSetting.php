<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Socialite\One\User;

class UserSetting extends Model
{
    protected $guarded = [];

    public function user()
    {
        $this->belongsTo(User::class);
    }
}
