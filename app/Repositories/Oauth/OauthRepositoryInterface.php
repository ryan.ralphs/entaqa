<?php


namespace App\Repositories\Oauth;

use Laravel\Socialite\Contracts\User as ProviderUser;

interface OauthRepositoryInterface
{
    public function getByProviderName(ProviderUser $providerUser, $provider);
}