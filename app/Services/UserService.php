<?php


namespace App\Services;

use App\Repositories\Oauth\OauthRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use Laravel\Socialite\Contracts\User as ProviderUser;

class UserService
{
    /**
     * @var OauthRepositoryInterface
     */
    protected $oauthRepository;

    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;

    /**
     * SocialAccountsService constructor.
     * @param OauthRepositoryInterface $oauthRepository
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(
        OauthRepositoryInterface $oauthRepository,
        UserRepositoryInterface $userRepository
    ) {
        $this->oauthRepository = $oauthRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param ProviderUser $providerUser
     * @param $provider
     * @return mixed
     */
    public function findOrCreateForProvider(ProviderUser $providerUser, $provider)
    {
        $account = $this->oauthRepository->getByProviderName($providerUser, $provider);

        if (!$account) {
            $account = new \stdClass();
            $account->user = $this->createForProvider($providerUser, $provider);
        }

        return $account->user;
    }

    /**
     * @param ProviderUser $providerUser
     * @param $provider
     * @return mixed
     */
    public function createForProvider(ProviderUser $providerUser, $provider)
    {

        $user = $this->userRepository->getUserByEmail($providerUser->getEmail());

        if (!$user) {
            $user = $this->userRepository->createFromProvider(
                $providerUser->getEmail(),
                $providerUser->getName()
            );
        }

        $this->userRepository->createUserAccount($user, $providerUser->getId(), $provider);

        return $user;
    }
}