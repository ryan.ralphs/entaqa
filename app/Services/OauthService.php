<?php

namespace App\Services;

use Laravel\Socialite\Facades\Socialite;

class OauthService
{
    /**
     * @var UserService
     */
    protected $userService;

    /**
     * OauthService constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param $provider
     * @return mixed
     */
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * @param $provider
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function callback($provider)
    {
        try {
            $user = Socialite::with($provider)->user();
        } catch (\Exception $e) {
            return redirect('/login');
        }

        $authUser = $this->userService->findOrCreateForProvider($user, $provider);

        auth()->login($authUser, true);

        return redirect()->to('/home');
    }

}